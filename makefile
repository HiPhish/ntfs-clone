# =========================================================================
# Copyright (C) 2016  Alejandro Sanchez
#
# NTFS-Clone is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# NTFS-Clone is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
# =========================================================================

# --- Public variables -------------------------------------------------------
PREFIX = './build'

# --- Internal variables -----------------------------------------------------
SRC_DIR = './src'
MAN_DIR = './man'

BINARIES = $(addprefix $(PREFIX)/bin/, ntfs-dump ntfs-restore)
MANPAGES = $(addprefix $(PREFIX)/man/, ntfs-dump.1 ntfs-restore.1)

# --- Phony targets ----------------------------------------------------------
.PHONY: all help bin man clean

all: bin man

bin: $(BINARIES)

man: $(MANPAGES)

help:
	@echo "Accepted make targets:"
	@echo "  all      Build all executables and manual pages (default)"
	@echo "  bin      Build binaries only"
	@echo "  man      Build manual pages only"
	@echo "  help     Display this information"
	@echo "  clean    Remove all build products"
	@echo ""
	@echo "Settable make variables:"
	@echo "  PREFIX   Where to place final build products (default './build')"

clean:
	@rm -rf ${PREFIX}


# --- Binaries ---------------------------------------------------------------
$(PREFIX)/bin/ntfs-dump: $(SRC_DIR)/ntfs-dump.sh
	@mkdir -p $(PREFIX)/bin/
	@cp $< $@
	@chmod +x $@

$(PREFIX)/bin/ntfs-restore: $(SRC_DIR)/ntfs-restore.sh
	@mkdir -p $(PREFIX)/bin/
	@cp $< $@
	@chmod +x $@

# --- Manpages ---------------------------------------------------------------
$(PREFIX)/man/ntfs-dump.1: $(MAN_DIR)/ntfs-dump.rst
	@mkdir -p $(PREFIX)/man/
	@rst2man.py $< $@

$(PREFIX)/man/ntfs-restore.1: $(MAN_DIR)/ntfs-restore.rst
	@mkdir -p $(PREFIX)/man/
	@rst2man.py $< $@
