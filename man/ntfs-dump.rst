###########
 ntfs-dump
###########

==================================================
Dump all the required images of an NTFS hard drive
==================================================

:Author: Alejandro Sanchez
:Date:   2016-05-30
:Copyright: 2016 Alejandro Sanchez
:Version: 1.0
:Manual section: 1
:Manual group: disc images



SYNOPSIS
########

ntfs-dump [<options>] <disc> <part-1> <part-2> ... <part-n>


DESCRIPTION
###########

Dump a set of disc image files required for producing perfect 1:1 clones of
NTFS hard drives. This tool will produce the images while 'ntfs-restore'
applies them.

The number of partitions is at least one, with no upper limit. The disc has to
be unmounted before dumping it. Options need to be specified before the
arguments. The script assumes that the file names of the partition- and disc
images are the same as their identifier (except for the file extension).

All images will be written to the directory specified by the '--dir' option. If
no option is specified the files will be written to the current working
directory.



OPTIONS
#######

The following options can be given in any order, but they must be given before
the positional arguments.

--dir <directory>
   Directory to dump the images to, the default is './'.


NOTE
####

The disc and all of its partitions must not be mounted. Depending on your
system 'ntfsclone' might need superuser permissions, so in that case you will
have to run this script as superuser.


SEE ALSO
########

* ntfs-restore
* fdisk
* dd
* ntfsclone
