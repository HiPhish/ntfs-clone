##############
 ntfs-restore
##############

=====================================================
Restore all the required images of an NTFS hard drive
=====================================================

:Author: Alejandro Sanchez
:Date:   2016-05-30
:Copyright: 2016 Alejandro Sanchez
:Version: 1.0
:Manual section: 1
:Manual group: disc images



SYNOPSIS
########

ntfs-restore [<options>] <disc> <part-1> <part-2> ... <part-n>


DESCRIPTION
###########

Restore a set of disc image files required for producing perfect 1:1 clones of
NTFS hard drives. This tool will apply the images while 'ntfs-dump' produces
them.

The number of partitions is at least one, with no upper limit. The disc has to
be unmounted before restoring it. Options need to be specified before the
arguments. The script assumes that the file names of the partition- and disc
images are the same as their identifier (except for the file extension).

All images will be read from the directory specified by the '--dir' option. If
no option is specified the files will be read from the current working
directory.



OPTIONS
#######

The following options can be given in any order, but they must be given before
the positional arguments.

--dir <directory>
   Directory to dump the images to, the default is './'.


NOTE
####

The disc and all of its partitions must not be mounted. Depending on your
system 'ntfsclone' might need superuser permissions, so in that case you will
have to run this script as superuser.


SEE ALSO
########

* ntfs-dump
* fdisk
* dd
* ntfsclone

