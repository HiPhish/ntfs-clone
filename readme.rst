.. default-role:: code

##################################################################
 NTFS-Clone: Create perfect 1:1 copies of NTFS hard discs on Unix
##################################################################

NTFS-Clone is  a collection  of Unix  shell  scripts  for creating  perfect 1:1
copies of NTFS hard discs on Unix.  If you have one PC installation and want to
run it on a number of similar PCs these  tools will allow you to clone the disc
and get your PCs running out of the box.


Motivation
##########

At my  workplace we  had a  number of  notebooks that  needed identical Windows
setups.  The workflow was to set up one PC,  make an image of its hard disc and
then  manually  apply  that  image to  every  other  PC  using  a  Windows  GUI
application.  This process was  very slow and tedious,  requiring one  of us to
babysit the machines all day.

This toolset was  created to automate  the process.  A suitable  solution would
have to meet the following requirements:

- Produce perfect 1:1 copies that don't need any Windows repair to be run
- Be able to clone multiple drives either in parallel or in sequence
- The cloning process must be able to be automated
- Ideally the solution would be Free Software

After a lot of searching  we found no solution that  would satisfy all of these
criteria.  In response I decided to do  it myself by doing things the Unix way:
combine a  number of  small programs  with very  specialised tasks  to solve  a
larger problem.


Requirements
############

The following  command-line  tools must  be present  on the  executing machine:
`fdisk`, `dd` and `ntfsclone`.  The latter is part of  the NTFS-3G_ packge from
Tuxera Inc. and is Free Software.

.. _NTFS-3G: http://www.tuxera.com/community/open-source-ntfs-3g/


Building
########

NTFS-Clone is a set of shell-scripts, so there is technically nothing to build,
but for the sake  of consistency there is  a makefile provided that  copies the
scripts to a `PREFIX`  directory and removes  their file extensions.  These are
the make targets available:

.. code-block:: sh

   make all      # Build the entire suite (default)
   make bin      # Build the scripts only
   make man      # Build the manual pages only
   make help     # Print build instructions
   make clean    # Remove all build products

You can  set the  `PREFIX`  (default  `./build/`)  variable to  have the  build
products be placed in a directory of your choice.

The manual pages  are written in reStructedText_ and  require `rst2man.py` from
Docutils_ installed to be compiled.

.. _reStructedText: http://docutils.sourceforge.net/rst.html
.. _Docutils: http://docutils.sourceforge.net


Usage
#####

NTFS-Clone consists of two programs:  `ntfs-dump` and `ntfs-restore`.  The task
of the former is to dump  all the necessary images  of the hard disc we wish to
clone while the  latter allows us  to apply these  images again.  Both programs
complement each other and each is the reverse of the other.  This is the reason
why their syntax is the same:

.. code-block:: sh

   ntfs-dump    [ --dir <directory> ] <disc> <part-1> <part-2> ... <part-n>
   ntfs-restore [ --dir <directory> ] <disc> <part-1> <part-2> ... <part-n>

Both have an optional directory  for specifying where to write the images to or
where to read from.  The positional  arguments are the  identifier for the disc
followed by the partitions.  Options must come before  the mandatory arguments.

Depending on  your operating  system some  of the  commands  contained in these
scripts might  require superuser privileges.  In that case you will have to run
them as superuser.

The file names are `<disc>.ptab` for the partition table,  `<disc>.mbr` for the
master boot record and `<part>.ntfs` for the individual partition.  `ntfs-dump`
will write files  under those names  `ntfs-restore` will  read the  files under
those names.


Acknowledgements
================

NTFS-Clone has been written based on the following articles:

- `<http://ebalaskas.gr/wiki/ntfsclone>`_
- `<http://bahut.alma.ch/2005/04/cloning-xp-with-linux-and-ntfsclone_23.html>`_


License
#######

**Copyright (C) 2016  Alejandro Sanchez**
 
NTFS-Clone is free software: you can redistribute it and/or modify it under the
terms of  the GNU  General Public  License as  published by  the Free  Software
Foundation,  either version  3  of the License,  or (at your option)  any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received  a copy of the GNU General Public  License along  with
this program.  If not, see `<http://www.gnu.org/licenses/>`_.
