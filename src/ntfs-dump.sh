#!/bin/sh
# Copyright (C) 2016  Alejandro Sanchez
# 
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.

print_usage() {
	cat <<- EOF
Usage: ntfs-dump [<options>] <disc> <part-1> <part-2> ... <part-n>

Options:
  --dir  <dir>   Directory to write the images to. Default './'

Positional Arguments:
  <disc>         Disc identifier of the disc to dump
  <part-1>       Partition identifier of the first partition
  <part-2>       Partition identifier of the second partition
  ...
  <part-n>       Partition identifier of the last partition

The number of partitions is at least one, with no upper limit. The disc has to
be unmounted before dumping it. Options need to be specified before the
arguments. The script assumes that the file names of the partition- and disc
images are the same as their identifier (except for the file extension).

NOTE:
  The disc and all of its partitions must not be mounted. Depending on your
  system 'ntfsclone' might need superuser permissions, so in that case you will
  have to run this script as superuser.
	EOF
}

# How it works: The user first specifies a number of optional options, followed
# by the identifier of the disc and a sequence of partitions. The options are
# processed first, setting the corresponding variables. After processing each
# argument we 'shift' the variables so that '$1' always contains the current
# option.
#
# After the options have been processed we do the same for the positional
# arguments where the first argument is the identifier of the disc and all
# remaining arguments are partitions. We will still keep shifting arguments so
# that '$1' always contains the identifier of the current partition.

# === Variables ===============================================================
DIR='./'
DISC=''

# === Processing Arguments ====================================================

# Parse options
while [ -n "$1" ]; do
	case $1 in
		--dir)  shift;  DIR=$1;;
		    *)  break;;
	esac
	shift
done

# If there are not enough arguments print usage instructions.
if [ $# -lt 2 ]; then print_usage; exit 1; fi
DISC="$1"
shift


# === Actual dumping process ==================================================

# Dump the permission table
fdisk -d /dev/"$DISC" > "$DIR"/"$DISC".ptab
if [ $? != 0 ] ; then
	echo >&2 "Error dumping partition table, aborting dumping process."
	return 1;
fi

# Dump the master boot record
dd if=/dev/"$DISC" of="$DIR"/"$DISC".ptab bs=512 count=1
if [ $? != 0 ] ; then
	echo >&2 "Error dumping master boot record, aborting process."
	return 2;
fi

# Dump all the partitions
while [ -n "$1" ]; do
	ntfsclone --save-image --output "$DIR"/"$1".ntfs /dev/"$1"
	if [ $? != 0 ] ; then
		echo >&2 "Error dumping partition $1, aborting dumping process."
		return 3;
	fi
	shift
done

return 0

